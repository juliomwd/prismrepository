﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using PrismEntity.Models;
using PrismEntity.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PrismEntity.ViewModels
{
    public class UsersCreateControlViewModel : BindableBase
    {
        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private IUnitOfWork _unit { get; set; }

        private DelegateCommand _saveCommand;
        public DelegateCommand SaveCommand =>
            _saveCommand ?? (_saveCommand = new DelegateCommand(ExecuteSaveCommand, CanExecuteSaveCommand)
            .ObservesProperty(() => User.Username)
            .ObservesProperty(() => User.Password)
            .ObservesProperty(() => User.Email));


        public InteractionRequest<INotification> NotificationRequest { get; set; }

        void ExecuteSaveCommand()
        {
            User.CreatedAt = new DateTime();
            _unit.Users.Add(User);
            _unit.Complete();
            User = new User();
        }

        bool CanExecuteSaveCommand()
        {
            if (String.IsNullOrWhiteSpace(User.Username) || String.IsNullOrWhiteSpace(User.Email) || String.IsNullOrWhiteSpace(User.Password))
            {
                return false;
            }
            return true;
        }

        public UsersCreateControlViewModel(IUnitOfWork unit)
        {
            _unit = unit;
            User = new User();
        }
    }
}
